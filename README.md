### Create config file
    $ cat > adapters.yml <<EOF
    endpoints:
        "POST /*/containers/create":
        pre: [logfiles]
        post: [logfiles]
    adapters:
        logfiles: http://logfiles/v1/extension
    EOF


### Run Powerstrip
    $ docker run --name powerstrip -ti \
    -v /var/run/docker.sock:/var/run/docker.sock \ 
    -v $PWD/adapters.yml:/etc/powerstrip/adapters.yml \ 
    -p 2375:2375 \
    --rm clusterhq/powerstrip

### Links
- https://github.com/infrabricks/powerstrip-demo