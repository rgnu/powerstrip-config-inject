var restify = require('restify');

var config = {
  "busybox": {
    Env: ['HELLO=World']
  }
} 

var server = restify.createServer({
  name: 'setup',
  version: '1.0.0'
});

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

server.post('/hello', function (req, res, next) {
  try {
    console.log("%s %s", req.body.ClientRequest.Request, req.body.ClientRequest.Method);

    var body = JSON.parse(req.body.ClientRequest.Body);

    body.Env.push('HELLO=World');

    req.body.ClientRequest.Body = JSON.stringify(body);

    res.send({
      "PowerstripProtocolVersion": req.body.PowerstripProtocolVersion,
      "ModifiedClientRequest": req.body.ClientRequest
    });

    return next();
  } catch (e) {
    return next(e)
  }
});

server.listen(3000, function () {
  console.log('%s listening at %s', server.name, server.url);
});
